$(document).ready(function() {
  $(".brand_logo .bx-menu-alt-right").click(function() {
    $("nav").addClass("show_nav");
  });
  $(".brand_logo + li").click(function() {
    $("nav").removeClass("show_nav");
  });
  $("nav li:not(.brand_logo)").click(function() {
    $("nav").removeClass("show_nav");
  });
  $(window).scroll(function() {
    if ($(this).scrollTop() > 0) {
      if ($(document).width() > 768) {
        if ($(this).scrollTop() < $("header").outerHeight() - 83) {
          $("nav").addClass('scrolled');
          $("nav .nav__overlay").addClass("nav__dark");
        }
        else {
          $("nav").addClass('scrolled');
          $("nav .nav__overlay").removeClass("nav__dark");
        }
      }
      else {
        if ($(this).scrollTop() < $("header").outerHeight() - $("nav").outerHeight()) {
          $("nav").addClass('scrolled');
          $("nav .nav__overlay").addClass("nav__dark");
        }
        else {
          $("nav").addClass('scrolled');
          $("nav .nav__overlay").removeClass("nav__dark");
        }
      }
    }
    else {
      $("nav .nav__overlay").removeClass('nav__dark');
      $("nav").removeClass("scrolled");
    }

    if ($(document).width() > 768) {
      if ($(this).scrollTop() > $("header").outerHeight() + $("#about_us").outerHeight() + $("#what_we_do").outerHeight() + $("#visi_misi").outerHeight() - 83) {
        if ($(this).scrollTop() > $("header").outerHeight() + $("#about_us").outerHeight() + $("#what_we_do").outerHeight() + $("#visi_misi").outerHeight() + $("#project").outerHeight() - 83) {
          $("nav .nav__overlay").removeClass('nav__dark');
        }
        else {
          $("nav .nav__overlay").addClass('nav__dark');
        }
      }
    }
    else {
      if ($(this).scrollTop() > $("header").outerHeight() + $("#about_us").outerHeight() + $("#what_we_do").outerHeight() + $("#visi_misi").outerHeight() - $("nav").outerHeight()) {
        if ($(this).scrollTop() > $("header").outerHeight() + $("#about_us").outerHeight() + $("#what_we_do").outerHeight() + $("#visi_misi").outerHeight() + $("#project").outerHeight() - $("nav").outerHeight()) {
          $("nav .nav__overlay").removeClass('nav__dark');
        }
        else {
          $("nav .nav__overlay").addClass('nav__dark');
        }
      }
    }
  });
  $(".radio_custom #project_event").change(function() {
    if ($(this).is(":checked")) {
      $("#project_event_item").addClass("show").removeClass("hide");
      $("#project_booth_item").addClass("hide").removeClass("show");
    }
    else {
      $("#project_event_item").removeClass("show").addClass("hide");
      $("#project_booth_item").removeClass("hide").addClass("show");
    }
  });
  $(".radio_custom #project_booth").change(function() {
    if ($(this).is(":checked")) {
      $("#project_event_item").addClass("hide").removeClass("show");
      $("#project_booth_item").addClass("show").removeClass("hide");
    }
    else {
      $("#project_event_item").removeClass("hide").addClass("show");
      $("#project_booth_item").removeClass("show").addClass("hide");
    }
  });
  $(".overlay_modal").click(function(e) {
    if (e.target !== this) return;
    $(this).removeClass("show").parents("body").removeAttr('style');
    $(this).find(".modal").removeClass("show");
  });
  //show event or booth description
  $("#project > .container figcaption > a").click(function(e) {
    e.preventDefault();
    var eventName = $(this).attr("id");
    if ($(".overlay_modal").find(".modal." + eventName).length == 1) {
      $(".overlay_modal").addClass("show").parents("body").css("overflow", "hidden");
      $(".overlay_modal").find(".modal." + eventName).addClass("show");
    }
  });
  $("footer time").text(new Date().getFullYear());
  //plugin slick
  $("#project .container #project_event_item > .col-12").slick({
    infinite: false,
    adaptiveHeight: false
  });
  $("#project .container #project_booth_item > .col-12").slick({
    infinite: false,
    adaptiveHeight: false
  });
  //end of plugin slick
});
